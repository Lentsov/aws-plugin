import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.javarubberduck"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.61"
    `maven-publish`
    `java-gradle-plugin`
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(platform("software.amazon.awssdk:bom:2.9.18"))
    implementation("software.amazon.awssdk:s3")
    implementation("software.amazon.awssdk:cloudformation")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testImplementation("org.jetbrains.kotlin:kotlin-reflect")
    testImplementation("org.mockito:mockito-all:2.0.2-beta")
}
repositories {
    mavenCentral()
    jcenter()
}

tasks.test {
    useJUnitPlatform()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

gradlePlugin {
    plugins {
        create("awsPlugin") {
            id = "com.javarubberduck.awsplugin"
            implementationClass = "com.javarubberduck.awsplugin.AwsPlugin"
        }
    }
}