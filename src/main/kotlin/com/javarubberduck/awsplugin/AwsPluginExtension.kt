package com.javarubberduck.awsplugin

import org.gradle.api.tasks.Input

open class AwsPluginExtension {
    @Input
    var s3BucketName: String = "projectName"
    @Input
    var profileName: String = "profileName"
    @Input
    var templatePath: String = "resourcePath"
    @Input
    var stackName: String = "stackName"
    @Input
    var region: String = "region"
}
