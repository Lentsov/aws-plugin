package com.javarubberduck.awsplugin.services

import com.javarubberduck.awsplugin.AwsPluginExtension

interface BucketService {
    fun upload(extension: AwsPluginExtension)
}