package com.javarubberduck.awsplugin.services

import com.javarubberduck.awsplugin.AwsPluginExtension
import com.javarubberduck.awsplugin.services.TemplateUtil.Companion.extractJarPath
import org.gradle.api.GradleScriptException
import org.gradle.api.InvalidUserDataException
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.BucketAlreadyOwnedByYouException
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import software.amazon.awssdk.services.s3.model.S3Exception
import java.io.File

class S3BucketService : BucketService {
    private lateinit var extension: AwsPluginExtension

    override fun upload(extension: AwsPluginExtension) {
        this.extension = extension
        createBucket()
        uploadJarFileToS3(extractJarPath(extension.templatePath))
    }

    private fun createBucket() {
        try {
            println("Create bucket operation is started\nbucket_name: ${extension.s3BucketName}\nprofile: ${extension.profileName}")
            createS3Client().use { client ->
                client.createBucket { requestBuilder ->
                    requestBuilder.bucket(extension.s3BucketName)
                }
            }
        } catch (e: BucketAlreadyOwnedByYouException) {
            println("${extension.s3BucketName} bucket is already owned by you. No need to create it")
        } catch (e: S3Exception) {
            throw GradleScriptException(e.localizedMessage, e)
        } catch (e: Throwable) {
            throw InvalidUserDataException("Create bucket operation failed with the following exception:\n $e")
        }
    }

    private fun uploadJarFileToS3(jarPath: String) {
        val requestBody = RequestBody.fromFile(File(jarPath))
        val request = PutObjectRequest.builder()
                .bucket(extension.s3BucketName).key("${extension.stackName}.jar").build()
        println("Here is everything ok")
        println(extension.s3BucketName)
        println(extension.stackName)
        println(extension.region)
        println(extension.profileName)
        try {
            createS3Client().putObject(request, requestBody)
        } catch (error: Throwable) {
            println(error)
            throw error
        }
    }

    private fun createS3Client(): S3Client {
        return S3Client.builder()
                .region(Region.of(extension.region))
                .credentialsProvider(ProfileCredentialsProvider.builder()
                        .profileName(extension.profileName)
                        .build())
                .build()
    }
}