package com.javarubberduck.awsplugin.services

import com.javarubberduck.awsplugin.AwsPluginExtension

interface DeploymentService {
    fun deployTemplate(extension: AwsPluginExtension)
}