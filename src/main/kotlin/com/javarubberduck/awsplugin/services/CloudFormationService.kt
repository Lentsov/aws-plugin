package com.javarubberduck.awsplugin.services

import com.javarubberduck.awsplugin.AwsPluginExtension
import org.gradle.api.InvalidUserDataException
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.cloudformation.CloudFormationClient
import software.amazon.awssdk.services.cloudformation.model.Capability
import software.amazon.awssdk.services.cloudformation.model.CloudFormationException
import software.amazon.awssdk.services.cloudformation.model.StackStatus


class CloudFormationService : DeploymentService {

    override fun deployTemplate(extension: AwsPluginExtension) {
        val cloudFormation = CloudFormationClient.builder()
                .region(Region.of(extension.region))
                .credentialsProvider(ProfileCredentialsProvider.builder()
                        .profileName(extension.profileName)
                        .build())
                .build()

        val template = TemplateUtil.readFileContent(extension.templatePath)

        cloudFormation.use { client ->
            validateTemplate(client, template)
            val stackExists = client.listStacks {
                it.stackStatusFilters(StackStatus.CREATE_COMPLETE)
            }.stackSummaries().any { it.stackName() == extension.stackName }

            if (stackExists) {
                println("Stack already exists $stackExists and will be updated")
                client.updateStack {
                    it.templateBody(TemplateUtil.replaceJarPathWithUri(extension.s3BucketName, extension.stackName, template))
                    it.capabilities(Capability.CAPABILITY_IAM, Capability.CAPABILITY_AUTO_EXPAND)
                    it.stackName(extension.stackName)
                } as Any
            } else {
                println("Creation of the new stack is started")
                client.createStack {
                    it.templateBody(TemplateUtil.replaceJarPathWithUri(extension.s3BucketName, extension.stackName, template))
                    it.capabilities(Capability.CAPABILITY_IAM, Capability.CAPABILITY_AUTO_EXPAND)
                    it.stackName(extension.stackName)
                } as Any
            }
        }
    }

    private fun validateTemplate(client: CloudFormationClient, template: String) {
        println("Template validation started")
        try {
            client.validateTemplate {
                it.templateBody(template)
            }
        } catch (e: CloudFormationException) {
            println(e.localizedMessage)
            throw InvalidUserDataException("Cloudformation script is not valid")
        }
        println("Template is valid")
    }
}