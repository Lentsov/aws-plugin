package com.javarubberduck.awsplugin.services

import org.gradle.api.InvalidUserDataException
import java.io.File
import java.util.regex.Pattern

class TemplateUtil {
    companion object {
        private const val jarPathPattern = "CodeUri:(.*)\n"

        @JvmStatic
        fun extractJarPath(templatePath: String): String {
            val template = readFileContent(templatePath)
            val pattern = Pattern.compile(jarPathPattern)
            val matcher = pattern.matcher(template)
            if (matcher.find()) {
                val jarPath = matcher.group(1).trim()
                println("JAR file $jarPath will be uploaded to s3 bucket")
                return jarPath
            } else {
                throw InvalidUserDataException("Could not find jar path")
            }
        }

        @JvmStatic
        fun readFileContent(filePath: String): String {
            return File(filePath).readLines().reduce { previousLine, nextLine -> previousLine + "\n" + nextLine }
        }

        @JvmStatic
        fun replaceJarPathWithUri(s3BucketName: String, stackName: String, template: String): String {
            return Regex(jarPathPattern).replace(template, "CodeUri: s3://$s3BucketName/$stackName.jar\n")
        }
    }
}