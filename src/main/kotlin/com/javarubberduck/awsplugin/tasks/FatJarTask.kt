package com.javarubberduck.awsplugin.tasks

import org.gradle.jvm.tasks.Jar

open class FatJarTask : Jar() {
    init {
        from(project.configurations.getByName("runtimeClasspath").map { config ->
            if (config.isDirectory) config as Any else project.zipTree(config) as Any })
        with(project.tasks.getByName("jar") as Jar)
    }
}