package com.javarubberduck.awsplugin.tasks

import com.javarubberduck.awsplugin.AwsPluginExtension
import com.javarubberduck.awsplugin.services.DeploymentService
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

open class DeployLambdaTask: DefaultTask() {
    @Input
    lateinit var deploymentService: DeploymentService
    @TaskAction
    fun deployLambda() {
        val extension = this.project.extensions.getByName("awsPlugin") as AwsPluginExtension
        deploymentService.deployTemplate(extension)
    }
}