package com.javarubberduck.awsplugin.tasks

import com.javarubberduck.awsplugin.AwsPluginExtension
import com.javarubberduck.awsplugin.services.BucketService
import com.javarubberduck.awsplugin.services.S3BucketService
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction


open class PackageLambdaTask: DefaultTask() {
    @Input
    lateinit var bucketService: BucketService
    @TaskAction
    fun packageLambda() {
        val extension = this.project.extensions.getByName("awsPlugin") as AwsPluginExtension
        bucketService.upload(extension)
    }
}