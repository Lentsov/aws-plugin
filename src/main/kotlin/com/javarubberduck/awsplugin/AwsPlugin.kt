package com.javarubberduck.awsplugin

import com.javarubberduck.awsplugin.services.CloudFormationService
import com.javarubberduck.awsplugin.services.S3BucketService
import com.javarubberduck.awsplugin.tasks.DeployLambdaTask
import com.javarubberduck.awsplugin.tasks.FatJarTask
import com.javarubberduck.awsplugin.tasks.PackageLambdaTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class AwsPlugin: Plugin<Project> {

    private val BUILD_FAT_JAR_TASK_NAME = "buildFatJar"
    private val PACKAGE_LAMBDA_TASK_NAME = "packageLambda"
    private val DEPLOY_LAMBDA_TASK_NAME = "deployLambda"

    override fun apply(target: Project) {
        createExtension(target)
        registerTasks(target)
        configurePackageLambdaTask(target)
        configureDeployLambdaTask(target)
        configureFatJarTask(target)
    }

    private fun createExtension(target: Project) {
        target.extensions.add("awsPlugin", AwsPluginExtension::class.java)
    }

    private fun registerTasks(target: Project) {
        target.tasks.register(BUILD_FAT_JAR_TASK_NAME, FatJarTask::class.java)
        target.tasks.register(PACKAGE_LAMBDA_TASK_NAME, PackageLambdaTask::class.java)
        target.tasks.register(DEPLOY_LAMBDA_TASK_NAME, DeployLambdaTask::class.java)
    }

    private fun configurePackageLambdaTask(target: Project) {
        val packageLambdaTask = target.tasks.getByName(PACKAGE_LAMBDA_TASK_NAME) as PackageLambdaTask
        packageLambdaTask.dependsOn(BUILD_FAT_JAR_TASK_NAME)
        packageLambdaTask.mustRunAfter(BUILD_FAT_JAR_TASK_NAME)
        packageLambdaTask.let{
            it.bucketService = S3BucketService()
            it.group = "aws management"
            it.description = "Creates S3 bucket if needed and uploads jar file to it"
        }
    }

    private fun configureDeployLambdaTask(target: Project) {
        val deployLambdaTask = target.tasks.getByName(DEPLOY_LAMBDA_TASK_NAME) as DeployLambdaTask
        deployLambdaTask.dependsOn(PACKAGE_LAMBDA_TASK_NAME)
        deployLambdaTask.mustRunAfter(PACKAGE_LAMBDA_TASK_NAME)
        deployLambdaTask.let {
            it.deploymentService = CloudFormationService()
            it.group = "aws management"
            it.description = "Creates aws stack according to cloudformation template"
        }
    }

    private fun configureFatJarTask(target: Project) {
        val fatJarTask = target.tasks.getByName(BUILD_FAT_JAR_TASK_NAME) as FatJarTask
        fatJarTask.let {
            it.group = "aws management"
            it.description = "Creates fat jar (java archive with all dependencies inside"
        }
    }
}