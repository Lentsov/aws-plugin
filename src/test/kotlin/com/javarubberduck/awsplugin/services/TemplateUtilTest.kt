package com.javarubberduck.awsplugin.services

import com.javarubberduck.awsplugin.services.TemplateUtil.Companion.extractJarPath
import com.javarubberduck.awsplugin.services.TemplateUtil.Companion.readFileContent
import com.javarubberduck.awsplugin.services.TemplateUtil.Companion.replaceJarPathWithUri
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class TemplateUtilTest {

    companion object {
        @JvmStatic
        private lateinit var template: File
        @JvmStatic
        private val incomingTemplate = """
AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'
Resources:
  KotlinLambdaFunction:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: kotlin-lambda
      Handler: handler.HelloWorldHandler::handleRequest
      Runtime: java8
      CodeUri: ./build/libs/kotlin-lambda-1.0-SNAPSHOT.jar
      Description: Hello world lambda written in Kotlin
      MemorySize: 512
      Timeout: 2
      Events:
        ApiGateway:
          Type: Api
          Properties:
            Path: /messages
            Method: GET"""
        @JvmStatic
        private val expectedTemplate = """
AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'
Resources:
  KotlinLambdaFunction:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: kotlin-lambda
      Handler: handler.HelloWorldHandler::handleRequest
      Runtime: java8
      CodeUri: s3://testBucket/testStack.jar
      Description: Hello world lambda written in Kotlin
      MemorySize: 512
      Timeout: 2
      Events:
        ApiGateway:
          Type: Api
          Properties:
            Path: /messages
            Method: GET"""

        @BeforeAll
        @JvmStatic
        fun setUpProject() {
            template = Files.createFile(Paths.get("./sam-template.yml")).toFile()
            template.writeText(incomingTemplate)
        }

        @AfterAll
        @JvmStatic
        fun cleanUp() {
            Paths.get("./sam-template.yml").toFile().delete()
        }
    }

    @Test
    fun `extract jar path properly`() {
        val extractedJarPath = extractJarPath("./sam-template.yml")
        assertEquals("./build/libs/kotlin-lambda-1.0-SNAPSHOT.jar", extractedJarPath)
    }

    @Test
    fun `read file content successfully`() {
        val extractedJarPath = readFileContent("./sam-template.yml")
        assertEquals(incomingTemplate, extractedJarPath)
    }

    @Test
    fun `replace jar path with s3 bucket uri`() {
        val processedTemplate = replaceJarPathWithUri("testBucket", "testStack", incomingTemplate)
        assertEquals(expectedTemplate, processedTemplate)
    }

}