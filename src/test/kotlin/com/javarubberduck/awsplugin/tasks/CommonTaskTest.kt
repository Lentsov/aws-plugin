package com.javarubberduck.awsplugin.tasks

import com.javarubberduck.awsplugin.AwsPluginExtension
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.BeforeEach

open class CommonTaskTest {

    protected lateinit var project: Project
    protected lateinit var awsPluginExtension: AwsPluginExtension

    @BeforeEach
    fun createProject() {
        project = ProjectBuilder.builder().build()
        awsPluginExtension = project.extensions.create("awsPlugin", AwsPluginExtension::class.java)
        awsPluginExtension.s3BucketName = "testProject"
        awsPluginExtension.profileName = "testProfile"
        awsPluginExtension.templatePath = "./resources/sam-template.yml"
        awsPluginExtension.stackName = "test"
        awsPluginExtension.region = "eu-west-1"
    }
}