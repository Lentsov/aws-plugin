package com.javarubberduck.awsplugin.tasks

import com.javarubberduck.awsplugin.services.BucketService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class PackageLambdaTaskTest: CommonTaskTest() {

    @Test
    fun `when task is started, bucket service is invoked with correct arguments`() {
        val packageLambdaTask = project.tasks.register("packageLambda", PackageLambdaTask::class.java)
                .get() as PackageLambdaTask

        val mockPackageService = mock(BucketService::class.java)
        packageLambdaTask.let {
            it.bucketService = mockPackageService
        }
        packageLambdaTask.packageLambda()
        verify(mockPackageService).upload(awsPluginExtension)
    }

}