package com.javarubberduck.awsplugin.tasks

import com.javarubberduck.awsplugin.AwsPluginExtension
import com.javarubberduck.awsplugin.services.DeploymentService
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito


class DeployLambdaTaskTest: CommonTaskTest() {

    @Test
    fun `when task is started, deployment service is invoked with correct arguments`() {
        val deployLambdaTask =
                project.tasks.register("deployLambda", DeployLambdaTask::class.java).get() as DeployLambdaTask

        val mockDeploymentService = Mockito.mock(DeploymentService::class.java)
        deployLambdaTask.deploymentService = mockDeploymentService

        deployLambdaTask.deployLambda()
        Mockito.verify(mockDeploymentService).deployTemplate(awsPluginExtension)
    }
}