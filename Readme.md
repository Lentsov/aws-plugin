This is the gradle aws plugin which is used to deploy sam template to the aws account. The reasoning behind it
and some explanation can be found in this article [https://javarubberduck.com/cloud/aws-gradle-plugin/]

Requirements:
- installed and configured aws-cli tool
- gradle as a build tool
- java 1.8
- kotlin 1.3.61

It is written mainly to help myself to deploy stack into aws account without need to use aws-cli directly.

